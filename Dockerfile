FROM adoptopenjdk/openjdk15
WORKDIR /
ADD target/HelloWorld-0.0.1-SNAPSHOT.jar HelloWorld.jar
EXPOSE 8080
CMD java - jar HelloWorld.jar